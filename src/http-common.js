import axios from "axios";

export default axios.create({
  baseURL: "http://localhost:9099/api/user",
  headers: {
    "Content-type": "application/json"
  }
});
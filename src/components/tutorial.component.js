import React, { Component } from "react";
import TutorialDataService from "../services/tutorial.service";
import { withRouter } from '../common/with-router';

class Tutorial extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangePrimaryPhoneNumber = this.onChangePrimaryPhoneNumber.bind(this);
    this.onChangeSecondPhoneNumber = this.onChangeSecondPhoneNumber.bind(this);
    this.getTutorial = this.getTutorial.bind(this);
    this.updateTutorial = this.updateTutorial.bind(this);
    this.deleteTutorial = this.deleteTutorial.bind(this);

    this.state = {
      currentTutorial: {
        id: null,
        name: "",
        primaryPhoneNumber: "",
        secondPhoneNumber: ""
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getTutorial(this.props.router.params.id);
  }

  onChangeName(e) {
    const name = e.target.value;

    this.setState(prevState => ({
      currentTutorial: {
        ...prevState.currentTutorial,
        name: name
      }
    }));
  }

  onChangePrimaryPhoneNumber(e) {
    const primaryPhoneNumber = e.target.value;
    
    this.setState(prevState => ({
      currentTutorial: {
        ...prevState.currentTutorial,
        primaryPhoneNumber: primaryPhoneNumber
      }
    }));
  }
  onChangeSecondPhoneNumber(e) {
    const secondPhoneNumber = e.target.value;
    
    this.setState(prevState => ({
      currentTutorial: {
        ...prevState.currentTutorial,
        secondPhoneNumber: secondPhoneNumber
      }
    }));
  }

  getTutorial(id) {
    TutorialDataService.get(id)
      .then(response => {
        this.setState({
          currentTutorial: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }


  updateTutorial() {
    TutorialDataService.update(
      this.state.currentTutorial
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The tutorial was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteTutorial() {    
    TutorialDataService.delete(
      this.state.currentTutorial.id
      )
      .then(response => {
        console.log(response.data);
        this.props.router.navigate('/tutorials');
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentTutorial } = this.state;

    return (
      <div>
        {currentTutorial ? (
          <div className="edit-form">
            <h4>Phone Book</h4>
            <form>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  value={currentTutorial.name}
                  onChange={this.onChangeName}
                />
              </div>
              <div className="form-group">
                <label htmlFor="primaryPhoneNumber">Primary Phone Number</label>
                <input
                  type="text"
                  className="form-control"
                  id="primaryPhoneNumber"
                  value={currentTutorial.primaryPhoneNumber}
                  onChange={this.onChangePrimaryPhoneNumber}
                />
              </div>

              <div className="form-group">
                <label htmlFor="secondPhoneNumber">Second Phone Number</label>
                <input
                  type="text"
                  className="form-control"
                  id="secondPhoneNumber"
                  value={currentTutorial.secondPhoneNumber}
                  onChange={this.onChangeSecondPhoneNumber}
                />
              </div>

              
            </form>

            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteTutorial}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updateTutorial}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Tutorial...</p>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(Tutorial);
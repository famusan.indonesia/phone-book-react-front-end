import React, { Component } from "react";
import TutorialDataService from "../services/tutorial.service";

export default class AddTutorial extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangePrimaryPhoneNumber = this.onChangePrimaryPhoneNumber.bind(this);
    this.onChangeSecondPhoneNumber = this.onChangeSecondPhoneNumber.bind(this);
    this.saveTutorial = this.saveTutorial.bind(this);
    this.newTutorial = this.newTutorial.bind(this);

    this.state = {
      id: null,
      name: "",
      primaryPhoneNumber: "", 
      secondPhoneNumber: "",
    };
  }

  onChangeName(e) {
    this.setState({
      name: e.target.value
    });
  }

  onChangePrimaryPhoneNumber(e) {
    this.setState({
      primaryPhoneNumber: e.target.value
    });
  }

  onChangeSecondPhoneNumber(e) {
    this.setState({
      secondPhoneNumber: e.target.value
    });
  }

  saveTutorial() {
    var data = {
      name: this.state.name,
      primaryPhoneNumber: this.state.primaryPhoneNumber,
      secondPhoneNumber: this.state.secondPhoneNumber
    };

    TutorialDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          name: response.data.name,
          primaryPhoneName: response.data.primaryPhoneName,
          secondPhoneName: response.data.secondPhoneName
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  newTutorial() {
    this.setState({
      id: null,
      name: "",
      primaryPhoneName: "",
      secondPhoneName: ""
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newTutorial}>
              Add
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={this.state.name}
                onChange={this.onChangeName}
                name="name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="primaryPhoneNumber">Primary Phone Number</label>
              <input
                type="text"
                className="form-control"
                id="primaryPhoneNumber"
                required
                value={this.state.primaryPhoneNumber}
                onChange={this.onChangePrimaryPhoneNumber}
                name="primaryPhoneNumber"
              />
            </div>

            <div className="form-group">
              <label htmlFor="secondPhoneNumber">Second Phone Number</label>
              <input
                type="text"
                className="form-control"
                id="secondPhoneNumber"
                required
                value={this.state.secondPhoneNumber}
                onChange={this.onChangeSecondPhoneNumber}
                name="secondPhoneNumber"
              />
            </div>


            <button onClick={this.saveTutorial} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}

import http from "../http-common";

class TutorialDataService {
  getAll() {
    return http.get("/");
  }

  get(id) {
    return http.get(`/id?id=${id}`);
  }

  create(user) {
    return http.post("/", user);
  }

  update(user) {
    return http.put("/",user);
  }

  delete(id) {
    return http.delete(`/id?id=${id}`);
  }

  findByName(name) {
    return http.get(`/name?name=${name}`);
  }
}

export default new TutorialDataService();